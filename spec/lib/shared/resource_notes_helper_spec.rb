# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/shared/resource_notes_helper'

RSpec.describe ResourceNotesHelper do
  let(:resource_klass) do
    Struct.new(:resource) do
      include ResourceNotesHelper
    end
  end

  let(:project_id) { 111 }
  let(:iid)        { 222 }
  let(:type)       { 'issues' }
  let(:comments)   { [] }

  subject { resource_klass.new(project_id: project_id, iid: iid, type: type) }

  describe '#has_unique_comment?' do
    before do
      stub_api_request(
        path: "/projects/#{project_id}/#{type}/#{iid}/notes",
        query: { per_page: 100 },
        response_body: comments
      )
    end

    context 'with issues' do
      context 'when issue notes has no unique comment' do
        it 'returns false' do
          expect(subject.has_unique_comment?('hello world')).to be false
        end
      end

      context 'when issue notes has comment from a non gitlab-bot author' do
        let(:comments) do
          [{ body: 'hello world!', author: { username: 'community-member' } }]
        end

        it 'returns false' do
          expect(subject.has_unique_comment?('hello world')).to be false
        end
      end

      context 'when issue notes has unique comment from gitlab-bot' do
        let(:comments) do
          [{ body: 'This comment contains hello world!',
             author: { username: 'gitlab-bot' } }]
        end

        it 'returns true' do
          expect(subject.has_unique_comment?('hello world')).to be true
        end
      end
    end

    context 'with merge requests' do
      let(:type) { 'merge_requests' }

      context 'with unique comment from gitlab-bot' do
        let(:comments) do
          [{ body: 'This comment contains hello world!',
             author: { username: 'gitlab-bot' } }]
        end

        it 'returns true' do
          expect(subject.has_unique_comment?('hello world')).to be true
        end
      end
    end
  end
end
